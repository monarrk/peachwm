CC = clang

X11PREFIX = /usr/X11R7

CFLAGS = -I${X11PREFIX}/include -L${X11PREFIX}/lib -lX11

all: pkd

pkd: pkd.c config.h
	${CC} -o pkd ${CFLAGS} pkd.c

run: all
	./pkd

fmt:
	clang-format -i *.h *.c

clean:
	rm -f pkd
