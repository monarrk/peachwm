// configuration for various things in peachwm

// keys
#define XK_Enter 0xff0d
#define XK_Q 0x0051
#define XK_F1 0xff91
#define XK_d 0x0064
#define XK_Left 0xff51
#define XK_Right 0xff53
#define XK_Up 0xff52
#define XK_Down 0xff54

// mask to use
#define MASK Mod4Mask

// quickly make a function to run a command
#define command(name, cmd) void name(Display* dpy, XEvent* ev) { system(cmd); }

// commands
command(rofi, "rofi -show run");
command(st, "st");

// peachwm commands
command(left, "pwm left");
command(right, "pwm right");
command(up, "pwm up");
command(down, "pwm down");

// keybindings
static Bind keybinds[] = {
	{ MASK, XK_F1,    focus }, // focus the window under the pointer
	{ MASK, XK_d,     rofi }, // execute rofi
	{ MASK, XK_Enter, st },
	// move windows
	{ MASK, XK_Left,  left },
	{ MASK, XK_Right, right },
	{ MASK, XK_Up,    up },
	{ MASK, XK_Down,  down },
};
