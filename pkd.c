#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int mask;
	KeySym key;
	void (*function)(Display*, XEvent*);
} Bind;

// func constructors
void grab_key(Display* dpy, Bind* bind);
void focus(Display* dpy, XEvent* ev);
void quit(Display* dpy, XEvent* ev);

// print to stderr
#define eprint(x) fprintf(stderr, x)

// get the size of an array
#define size(x) (sizeof(x) / sizeof(*x))

// check if strings match
#define matches(x, y) (strcmp((x), (y)) == 0)

#include "config.h"

// grab a keybind
void grab_key(Display* dpy, Bind* bind) {
	XGrabKey(dpy, XKeysymToKeycode(dpy, bind->key), bind->mask, DefaultRootWindow(dpy), True, GrabModeAsync, GrabModeAsync);
}

// focus a window
void focus(Display* dpy, XEvent* ev) {
	if (ev->xkey.subwindow == None) return;
	
	XRaiseWindow(dpy, ev->xkey.subwindow);
	XSetInputFocus(dpy, ev->xkey.subwindow, RevertToNone, CurrentTime);
	// XSetWindowBorder(dpy, ev->xkey.subwindow, 
}

// quit pkd
void quit(Display* dpy, XEvent* ev) {
	exit(0);
}

int main(void) {
	Display* dpy;
	XEvent ev;

	// connect to the X server
	if (!(dpy = XOpenDisplay(0x0))) {
		eprint("Error opening X display.\n");
		return 1;
	}

	// grab the keybinds in config.h
	for (int i = 0; i < size(keybinds); i++) {
		grab_key(dpy, &(keybinds[i]));
	}

	for (;;) {
		XNextEvent(dpy, &ev);
		if (ev.type == KeyPress) {
			int keysyms_per_keycode;
			KeySym* key = XGetKeyboardMapping(dpy, ev.xkey.keycode, 1, &keysyms_per_keycode);

			for (int i = 0; i < size(keybinds); i++) {
				Bind* bind = &(keybinds[i]);
				if (bind->key == *key) {
					bind->function(dpy, &ev);
				}
			}
		}
	}
}
